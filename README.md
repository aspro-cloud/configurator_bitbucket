# configurator_bitbucket #

Скрипт сгенерирует ssh-ключи для доступа к Bitbucket

### Как использовать? ###

* Запустить скрипт start_<Ваша ОС>.sh
* Придумать и ввести кодовую фразу (passphrase)
* Скопировать из консоли публичный ssh-ключ
* Добавить публичный ssh-ключ на Bitbucket в Personal settings -> Account settings -> SSH keys
* Проверить доступ к Bitbucket, выполнив команду "ssh -T git@bitbucket.org"
