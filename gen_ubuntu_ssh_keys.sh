#!/bin/bash

mkdir -p ~/.ssh

ssh-keygen -f ~/.ssh/id_rsa

if [ -f ~/.ssh/id_rsa ] && [ -f ~/.ssh/id_rsa.pub ]
then
    ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts
    echo -e "\n\n\n\n\n"
    echo -e "[ OK ] Генерация ssh-ключей завершена успешно\n"
    echo -e "Добавить публичный SSH-ключ на Bitbucket в Personal settings -> Account settings -> SSH keys\n"
    echo -e "Ключ ниже и начинается с ssh-rsa:\n"
    cat ~/.ssh/id_rsa.pub
    echo -e "\n"
else
    echo -e "\n\n\n\n\n"
    echo -e "[FAIL] Генерация ssh-ключей завершена с ошибкой\n"
    echo -e "Сгенерируйте ssh-ключи другим способом\n"
    exit 1
fi
